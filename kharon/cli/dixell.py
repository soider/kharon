from __future__ import absolute_import
"""CLI entry point for move_psql_to_sql log converter"""
import logging

from kharon.servicehubs.dixell import convert


def main():
    logger = logging.getLogger("cli.dixell")
    logger.info("Get ready")
    return convert(logger)

if __name__ == "__main__":
    main()
