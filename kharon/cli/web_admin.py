from __future__ import absolute_import
"""CLI web admin starter"""
import os

import gunicorn
from werkzeug.contrib.fixers import ProxyFix

from kharon.web import app

app.app.wsgi_app = ProxyFix(app.app.wsgi_app)

port = os.environ.get("KHARON_WEB_ADMIN_PORT", 8080)
host = os.environ.get("KHARON_WEB_ADMIN_HOST", "0.0.0.0")

def main():
    app.app.run(host, long(port))

if __name__ == "__main__":
    main()
