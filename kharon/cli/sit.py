from __future__ import absolute_import
"""CLI entry point for move_ms_to_sql log converter"""
import logging

from kharon.servicehubs.sit import convert


def main():
    logger = logging.getLogger("cli.sit")
    logger.info("Get ready")
    return convert(logger)

if __name__ == "__main__":
    main()
