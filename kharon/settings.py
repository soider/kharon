"""Provide settings"""
import logging

DEBUG = True

# DBs

DATABASES = {
    "mysql_log": {
        "connection_type": "mysql",
        "params": {
            "host": "localhost",
            "user": "root",
            "database": "esavings"
        }
    },
    "meta": {
        "connection_type": "mysql",
        "params": {
            "host": "localhost",
            "user": "root",
            "database": "esavings",
            "charset": "utf-8",
            "use_unicode": True
        }
    }
}

DATABASES["default"] = DATABASES["mysql_log"]

WEB_SETTINGS = dict(
    DATABASE = {
        "name": "",
        "engine": "peewee.MySQLDatabase",
        "user": "",
        "host": "",
        "passwd": "",
        "threadlocals": True
    },
    DATABASE_ORIG = {
        "name": "",
        "engine": "peewee.MySQLDatabase",
        "user": "",
        "host": "",
        "passwd": "",
        "threadlocals": True
    },
    DEBUG = DEBUG,
    SECRET_KEY = "1231231asafjndf")

PSQL_LOG_TABLE = {
    "begin_table_name": "hsvar_060_01",
    "table_name_tpl": "hsvar_060_{0}"
}

MS_SQL_LOG_TABLE = {
    "table_name": "data"
}

MS_SQL_CHARSET = "UTF-8"

INSERT_TABLE = "pdata"

if DEBUG:
    DEFAULT_LEVEL = logging.DEBUG
else:
    DEFAULT_LEVEL = logging.ERROR

default_formatter = logging.Formatter('%(asctime)s \
- %(name)s \
- %(levelname)s \
- %(message)s')

LOGGERS = {
        "cli": {
            "level": DEFAULT_LEVEL,
            "handlers": [
                (logging.StreamHandler(),
                 default_formatter),
            ]},
        "cli.carel": {
            "level": DEFAULT_LEVEL,
            "handlers": [
                (logging.FileHandler("/tmp/carel.log"), default_formatter)
            ]},
        "cli.sit": {
            "level": DEFAULT_LEVEL,
             "handlers":  [(logging.FileHandler("/tmp/sit.log"), default_formatter)
            ]},
        "cli.danfoss": {
            "level": DEFAULT_LEVEL,
             "handlers":   [(logging.FileHandler("/tmp/danfoss.log"), default_formatter)
            ]},
        "cli.dixell": {
            "level": DEFAULT_LEVEL,
             "handlers":   [(logging.FileHandler("/tmp/dixell.log"), default_formatter)
            ]},
    }


MAX_DIXELL_THREADS = 50
DIXELL_TIMEOUT = 10

# Try to override default settings
try:
    import kharon_loggers
except ImportError:
    pass
else:
    default_loggers = LOGGERS
    from kharon_loggers import LOGGERS


# Try to override default settings
try:
    import kharon_conf
except ImportError:
    pass
else:
    default_settings = globals()
    WEB_SETTINGS = default_settings['WEB_SETTINGS'].update(kharon_conf.WEB_SETTINGS)
    from kharon_conf import *
