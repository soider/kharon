from datetime import datetime
from dateutil.relativedelta import relativedelta

from kharon.errors import BadDatabaseCondition
from kharon.settings import PSQL_LOG_TABLE


def get_current_log_table_name(connection, first_db):
    """
    Find table name for current day

    :param connection:
    :type db-api 2.0 connection object:

    :param first_db:
    :type string:
    :desc first table name:

    :return string
    """
    query = """
        SELECT DISTINCT DATE_TRUNC('month', "timestamp") FROM {0}
    """.format(first_db)
    cursor = connection.cursor()
    cursor.execute(query)
    begin = cursor.fetchone()
    if begin is None:
        raise BadDatabaseCondition(
            "No records in first log table, check db and config")
    begin = begin[0]
    current_time = datetime.now()
    time_delta = relativedelta(begin, current_time)
    month_count = abs(time_delta.years) * 12 + abs(time_delta.months) + 1
    table_name = PSQL_LOG_TABLE["table_name_tpl"].format(month_count)
    return table_name
