# *-* encoding: utf-8 *-*
import traceback

from kharon import settings
from kharon.db.connect import get_database_connection, create_connection

from dateutil.relativedelta import relativedelta


def get_meta_information(connection):
    """
    :param connection:
    :type db-api 2.0 connection:

    :return list of tuples
    """
    query = """
    SELECT
        id,
        sit_host,
        sit_user,
        sit_password,
        sit_db,
        sit_devcode,
        sit_parnumber,
        esavings_mdata_meterid
    FROM sit
    WHERE active = 1
    """
    cur = connection.cursor()
    cur.execute(query)
    return cur.fetchall() or []


def get_ms_connection():
    """Stub"""
    params = {
        "connection_type": "mssql",
        "params": {
            "host": "172.31.31.65",
            "user": "sa",
            "password": "isE#32br",
            "database": "P2kX5RetailGroup",
            "charset": settings.MS_SQL_CHARSET
        }
    }
    return create_connection(**params)


def get_records(connection, table_name, parnumber, device_code, logger):
    """

    :param connection
    :type db-api 2.0 connection

    :param table_name
    :type string

    :param parnumber
    :type integer

    :param device_code
    :type integer

    :param logger
    :type logging.Logger

    :return list of tuples
    """
    cursor = connection.cursor()
    query = """

    SELECT data.data_date, data.value0
    FROM data
    INNER JOIN devices ON data.object=devices.code
    WHERE
        parnumber=%s -- метадата
        AND
        devices.code=%s
	AND
	data.ITEM=1
        AND data.data_date
            BETWEEN
            DATEADD(DAY, -2, GETDATE())
            AND
            DATEADD(DAY, -1, GETDATE())
    ORDER BY
        devices.code, data.data_date
    """.format(table_name)
    cursor.execute(query, (parnumber, device_code))
    return cursor.fetchall()


def insert_records(connection, logger, records, mdata):
    cursor = connection.cursor()
    query = """
        INSERT INTO -- paramid из мета
            {0} (date, value, meterid, paramid, tariffid)
        VALUES
            (%s, %s, %s, 6, 1)
    """.format("mdata")
    for idx, (date, value) in enumerate(records):
        records[idx] = (date, value, mdata)
    count = cursor.executemany(query, records)
    if records:
        try:
            ident = records[0][0].isoformat()
        except Exception:
            ident = str(records[0])
        logger.info("Successfully inserted %s bulks for %s",
                    count, ident)
        connection.commit()
    return count


def update_meta_information(connection, meta_id):
    """

    :param connection:
    :type db-api 2.0 connection:

    :param meta_id:
    :type integer:
    :desc sql key for update:

    """

    query = """
    UPDATE sit
    SET lastsync=now()
    WHERE id = %s
    """

    cur = connection.cursor()
    cur.execute(query, (meta_id,))
    connection.commit()


def convert(logger):
    """Entry point"""
    mysql_connection = get_database_connection("mysql_log")
    meta_connection = get_database_connection("meta")
    log_table = settings.MS_SQL_LOG_TABLE["table_name"]
    try:
        for element in get_meta_information(meta_connection):
            try:
                logger.info(
                    "Begin with sit element %s", element[:3] + element[4:])
                params = dict(zip(["host", "user", "password", "database"],
                                  element[1:5]))
                params["charset"] = settings.MS_SQL_CHARSET
                connection = create_connection("mssql", params)
                records = get_records(
                    connection, log_table, element[6], element[5], logger)
                count = insert_records(mysql_connection, logger, records, element[7])
            except Exception as error:
                logger.error("Error happened with element %s", element)
                logger.error(traceback.format_exc())
            else:
                try:
                    update_meta_information(meta_connection, element[0])
                except Exception as meta_error:
                    logger.error(
                        "Error while updating meta information for %s",
                        element[0])
                    logger.error(traceback.format_exc())
            finally:
                connection.close()
    finally:
        meta_connection.close()
        mysql_connection.close()
