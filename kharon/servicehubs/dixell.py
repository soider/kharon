# *-* coding: utf-8 *-*
import sys
import traceback
import urllib2
import datetime
import lxml.etree as ET # no more fucking standart library xml! Fuck it!

from kharon import settings
from kharon.db.connect import get_database_connection

from kharon.servicehubs.carel import insert_record

from gevent import monkey; monkey.patch_socket()
from gevent import queue
import gevent


def get_response(url, timeout, logger):
    try:
        return urllib2.urlopen(url, timeout=timeout).read()
    except urllib2.URLError:
        logger.warning("Timeout with %s, skip!", url)
        return None

def get_meta_information(connection, mode):
    """
    :param connection:
    :type db-api 2.0 connection:

    :return list of tuples
    """

    query = """
    SELECT
        id,
        dixell_url,
        type,
        dixell_value_name,
        esavings_paramid,
        esavings_unitid,
        esavings_mdata_meterid,
        esavings_mdata_paramid,
        esavings_mdata_last
    FROM dixell
    WHERE active = 1
            AND
          type = %s
    """
    cur = connection.cursor()
    cur.execute(query, (mode,))
    return cur.fetchall() or []

def update_meta_information(connection, target, new_value):
    query = """
    UPDATE dixell
    SET lastsync=now(),
    esavings_mdata_last = %s
    WHERE id = %s
    """
    cur = connection.cursor()
    cur.execute(query, (new_value, target[0]))
    connection.commit()


def make_target_bulcs(targets, threads_count):
    """Spllit """
    targets = list(targets) # Do not modify original object
    if len(targets) <= threads_count:
        bulc_size = 1
        threads_count = len(targets)
    else:
        bulc_size = len(targets) / threads_count
    return [[targets.pop() for x in range(bulc_size)] for y in range(threads_count)]


def handle_bulc(targets, logger, mysql_connection, meta_connection):
    for target in targets:
        try:
            logger.info("begin with %s", target[:1] + target[2:])
            (_id,
                response,
                mode,
                param_name,
                param_id,
                unit_id,
                mmeterid,
                mparam_id,
                old_value) = target
            try:
                answer = ET.fromstring(response)
            except:
                logger.error("Error while parsing answer %s", response, exc_info=1)
            else:
                for elem in answer.xpath("""//device[analogInput/name/node() = '"%s"']""" % param_name):
                    new_value = float(elem.xpath("""analogInput[name/node() = '"%s"']/value/node()""" % param_name)[0].strip('"'))
                    if mode == "temp":
                        record = (param_id,
                                    datetime.datetime.now(),
                                    new_value,
                                    unit_id
                                    )
                    else:
                        if old_value:
                            value = new_value - old_value
                        else:
                            logger.info("First time, skip")
                            value = new_value
                            continue

                        record = (value,
                                    datetime.datetime.now(),
                                    mmeterid,
                                    mparam_id
                                    )
                    logger.info("Will insert %s for type %s", record, mode)
                    insert_record(mysql_connection,
                                    logger,
                                    [record],
                                    mode)
        except:
            logger.error("Unknown error with", exc_info=1)
        else:
            try:
                update_meta_information(meta_connection, target, new_value)
            except Exception as err:
                logger.error(
                    "Error while updating meta information for %s",
                    target,
                    exc_info=1
                )

def get_responses_bulc(targets, logger):
    results = []
    try:
        for target in targets:
            url = target[1]
            try:
                response = get_response(url, timeout=settings.DIXELL_TIMEOUT, logger=logger)
                if response is None:
                    continue
            except Exception:
                logger.error("Error while sending request %s", url, exc_info=1)
            else:
                result = list(target)
                result[1] = response
                results.append(tuple(result))
    finally:
        return results


def convert(logger):
    """Dixell entry point"""
    if not sys.argv[1:]:
        mode = "temp"
    else:
        mode = sys.argv[1]
    meta_connection = get_database_connection("meta")
    mysql_connection = get_database_connection("mysql_log")
    bulcs_of_targets = make_target_bulcs(get_meta_information(meta_connection, mode), settings.MAX_DIXELL_THREADS)
    try:
        responses = []
        greenlets = []
        q = queue.JoinableQueue()
        for targets in bulcs_of_targets:
            q.put(targets)
        def worker(id):
            logger.debug("Start greenlet %s", id)
            targets = q.get()
            try:
                result = get_responses_bulc(targets, logger)
            finally:
                q.task_done()
            logger.debug("End greenlet %s", id)
            return result

        for x in range(len(bulcs_of_targets)):
            greenlets.append(gevent.spawn(worker, (x)))

        q.join()
        results = []
        for greenlet in greenlets:
            if greenlet.successful():
                results.extend(greenlet.value)
        # I don't know how correct pymysql work with monkey-patched sockets
        # so write to mysql in one main thread after all greenlets finished
        handle_bulc(results, logger, mysql_connection, meta_connection)
    finally:
        meta_connection.close()
        mysql_connection.close()



