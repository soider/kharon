import traceback
import urllib2
import urlparse
import xml.dom.minidom
import datetime
import xml.etree.ElementTree as ET

from xml.parsers.expat import ExpatError
from kharon import settings
from kharon.db.connect import get_database_connection


def get_meta_information(connection):
    """
    :param connection:
    :type db-api 2.0 connection:

    :return list of tuples
    """

    query = """
    SELECT
        id,
        danfoss_host,
        danfoss_node,
        danfoss_cid,
        danfoss_vid,
        esavings_unitid,
        esavings_paramid
    FROM danfoss
    WHERE active = 1
    """
    cur = connection.cursor()
    cur.execute(query)
    return cur.fetchall() or []


def build_command(cmd):
    cmd_root = xml.dom.minidom.Element("cmd")
    default_attrs = [
        ("compress", 0),
        ("num_only", 1),
        ("units", "S"),
        ("action", str(cmd))
    ]
    for (attr, value) in default_attrs:
        cmd_root.attributes[attr] = str(value)
    return cmd_root


def build_read_val_command(nodes):
    cmd_root = build_command("read_val")
    for (node, cid, vid) in nodes:
        param = xml.dom.minidom.Element("val")
        param.attributes["nodetype"] = "16"
        param.attributes["node"] = str(node)
        param.attributes["cid"] = str(cid)
        param.attributes["vid"] = str(vid)
        cmd_root.appendChild(param)
    return cmd_root


def send_command(host, command):
    url = urlparse.urlunparse(("http", host, "/html/xml.cgi", None, None, None))
    request = urllib2.urlopen(url, command.toxml())
    return request


def insert_record(connection, unit_id, param_id, xml_response, date, cid, vid, logger):
    query = """
        INSERT INTO
            {0} (paramid, datat, value, unitid)
        VALUES
            (%s, %s, %s, %s)
    """.format("pdata")
    cursor = connection.cursor()
    count = 0
    for val in xml_response.findall("val"):
        try:
            if "error" in val.attrib:
                logger.error("There is an error attribute in answer %s", str(val.attrib))
            else:
                try:
                    temp_value = float("".join([x for x in val.text if x.isdigit() or x in (".", "-")]))
                except TypeError:
                    logger.warning("No value in answer %s", str(val.attrib))
                else:
                    try:
                        c = cursor.execute(query, (param_id, date, temp_value, unit_id))
                        count += c
                        connection.commit()
                    except Exception:
                        logger.error("Error while inserting record %s %s %s %s %s", param_id, date, temp_value, unit_id, str(val.attrib))
                        logger.error(traceback.format_exc())
        except Exception:
            logger.error("Error while working with %s %s %s %s %s", param_id, date, temp_value, unit_id, str(val.attrib))
            logger.error(traceback.format_exc())
    logger.info("OK %s", count)

def update_meta_information(connection, target):
    query = """
    UPDATE danfoss
    SET lastsync=now()
    WHERE id = %s
    """
    cur = connection.cursor()
    cur.execute(query, (target[0],))
    connection.commit()


def convert(logger):
    meta_connection = get_database_connection("meta")
    mysql_connection = get_database_connection("mysql_log")
    targets = get_meta_information(meta_connection)
    try:
        for target in targets:
            logger.info("begin with %s", target)
            try:
                (_id, host, node, cid,
                    vid, unit_id, param_id) = target
                command = build_read_val_command([(node, cid, vid)])
                try:
                    response = send_command(host, command)
                except Exception as err:
                    logger.error(
                        "Error while sending post\
request to %s to node %s cid: %s vid: %s",
                        host, node, cid, vid)
                    logger.error(traceback.format_exc())
                else:
                    try:
                        answer = response.read()
                        xml_response = ET.fromstring(answer)
                    except ET.ParseError:
                        logger.error(
                            "Invalid xml %s node %s cid: %s vid: %s",
                            host, node, cid, vid)
                        logger.error(answer)
                        logger.error(traceback.format_exc())
                    except Exception:
                        logger.error(
                            "Unknown error %s node %s cid: %s vid: %s",
                            host, node, cid, vid)
                        logger.error(answer)
                        logger.error(traceback.format_exc())
                    else:
                        try:
                            insert_record(
                                mysql_connection,
                                unit_id,
                                param_id,
                                xml_response,
                                datetime.datetime.now(),
                                cid,
                                vid,
                                logger
                                )
                        except Exception as err:
                            logger.error("Error while inserting record")
                            logger.error(answer)
                            logger.error(traceback.format_exc())
            except Exception as err:
                logger.error(
                        "Error while working with %s",
                        target
                    )
                logger.error(traceback.format_exc())
            else:
                try:
                    update_meta_information(meta_connection, target)
                except Exception as err:
                    logger.error(
                        "Error while updating meta information for %s",
                        target
                    )
                    logger.error(traceback.format_exc())
    finally:
        meta_connection.close()
        mysql_connection.close()
