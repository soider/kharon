import traceback

from kharon import settings
from kharon.db.connect import get_database_connection, create_connection
from kharon.tools.db import get_current_log_table_name

from dateutil.relativedelta import relativedelta

import math


def get_table_name():
    """
    Create table name for current day

    :return string
    """
    return get_current_log_table_name(
        get_database_connection("psql_log"),
        settings.PSQL_LOG_TABLE["begin_table_name"]
    )


def get_records(connection, table_name, supervisor, variable, logger):
    """
    Return all records from log table for previous day

    :param connection
    :type db-api 2.0 connection

    :param table_name:
    :type string:

    :param supervisor:
    :type integer:

    :param variable:
    :type integer:

    :param logger:
    :type logging.Logger:

    :return list of tuples
    """
    query = """
        SELECT * from {0}
        WHERE
            timestamp::DATE
		BETWEEN
			(NOW() - INTERVAL '2 day')::DATE
		AND
			(NOW() - INTERVAL '1 day')::DATE
        AND
            value001 != 'NaN' -- #Fix: Need this?
        AND (idsupervisor,idvariable) IN (values (%s,%s))
        ORDER BY
            idsupervisor, idvariable, timestamp, lasttime
        -- LIMIT 3 -- #TODO: Remove limit
    """.format(table_name)
    cur = connection.cursor()
    logger.debug(cur.mogrify(query, (supervisor, variable)))
    cur.execute(query, (supervisor, variable))
    return cur.fetchall()


def convert_record(record, unit, param, mode, meterid):
    """
    Convert psql log record to mysql records bulk

    :param record:
    :type psql tuple:
    :return list of tuples:

    """
    param_id = param
    unit_id = unit
    timestamp = record[2]
    values = record[4:]
    result = []

    def convert_power():
        n_values = list(values[::30]) + [values[-1]] # use only half hour values

        for idx, value in enumerate(values):
            if idx in (29, 59, 89, 119):
                delta = relativedelta(minutes=idx)
                if idx == 29:
                    prev_idx = 0
                else:
                    prev_idx = idx - 30
                result.append(
                    (value - values[prev_idx], timestamp + delta, meterid, 6)
                )

    def convert_temp():
        for idx, value in enumerate(values):
            delta = relativedelta(minutes=idx)
            result.append(
                (param_id, timestamp + delta, value, unit_id)
            )

    locals()["convert_%s" % mode]()
    return result


def insert_record(connection, logger, records, mode):
    """
    Insert log records dispatcher

    :param connection:
    :type db-api 2.0 connection:

    :param logger:
    :type logging.Logger:

    :param records:
    :type list of 4 element tuple:

    :param mode
    :type string
    :desc temp|power

    :return count:
    """
    func = insert_record_power
    if mode == "temp":
        func = insert_record_temp

    def nan_to_none(el):
        if type(el) == float:
            if not math.isnan(el):
                return el
        else:
            return el

    records = [[nan_to_none(col) for col in record] for record in records]
    count = func(connection, logger, records)
    logger.info("Successfully inserted %s bulks for %s",
                count, records[0][1].isoformat())
    connection.commit()
    return count

def insert_record_power(connection, logger, records):
    """Insert power records"""
    logger.info("Inserting power")
    query = """
        INSERT INTO -- paramid
            {0} (value, date, meterid, paramid, tariffid)
        VALUES
            (%s, %s, %s, %s, 1)
    """.format("mdata")
    cur = connection.cursor()
    return cur.executemany(query, records)


def insert_record_temp(connection, logger, records):
    """
    Insert temperature records
    """
    logger.info("Inserting temp")
    query = """
        INSERT INTO
            {0} (paramid, datat, value, unitid)
        VALUES
            (%s, %s, %s, %s)
    """.format("pdata")
    cur = connection.cursor()
    return cur.executemany(query, records)


def get_meta_information(connection):
    """
    :param connection:
    :type db-api 2.0 connection:

    :return list of tuples
    """
    query = """
    SELECT id,
           carel_host, carel_user, carel_password,
           carel_db, carel_idsupervisor,
           carel_idvariable, esavings_unitid,
           esavings_paramid, type, esavings_mdata_meterid
    FROM carel
    WHERE active = 1
    """
    cur = connection.cursor()
    cur.execute(query)
    return cur.fetchall() or []


def update_meta_information(connection, meta_id):
    """
    :param connection:
    :type db-api 2.0 connection:

    :param meta_id:
    :type integer:
    :desc sql key for update:

    """
    query = """
    UPDATE carel
    SET lastsync=now()
    WHERE id = %s
    """
    cur = connection.cursor()
    cur.execute(query, (meta_id,))
    connection.commit()


def convert(logger):
    """Entry point"""
    mysql_connection = get_database_connection("mysql_log")
    meta_connection = get_database_connection("meta")
    log_table = get_table_name()
    meta_information = get_meta_information(meta_connection)

    try:
        for element in meta_information:
            try:
                mysql_records = []
                logger.info(
                    "Begin with carel element %s", element[:3] + element[4:])
                params = dict(zip(["host", "user", "password", "database"],
                                  element[1:5]))
                connection = create_connection("psql", params)
                supervisor, variable, unit, param, mode, meterid = element[5:]

                records = get_records(
                    connection, log_table, supervisor, variable, logger)
                mysql_records.extend(map(lambda x: convert_record(
                    x, unit, param, mode, meterid), records))
                insert_function = lambda el: insert_record(
                    mysql_connection, logger, el, mode)
                results = map(insert_function, mysql_records)
                for idx, bulk in enumerate(results):
                    if bulk != 120 and bulk != 4:
                        logger.warning("Inserted less then 120 bulks for %s",
                                       records[idx])
            except Exception as error:
                logger.error("Error happened with element %s", element)
                logger.error(traceback.format_exc())
            else:
                try:
                    update_meta_information(meta_connection, element[0])
                except Exception as meta_error:
                    logger.error(
                        "Error while updating meta information for %s",
                        element[0])
                    logger.error(traceback.format_exc())
            finally:
                connection.close()
    finally:
        meta_connection.close()
        mysql_connection.close()
