from psycopg2 import connect as connect_psql
from MySQLdb import connect as connect_mysql
from pymssql import connect as connect_mssql

from kharon import settings


CACHE = {}


def get_database_connection(connection_key=None):
    """Get connection from cache or create it"""
    if connection_key in CACHE:
        try:
            if CACHE[connection_key].closed:
                del CACHE[connection_key]
        except AttributeError:
            if not CACHE[connection_key].open:
                del CACHE[connection_key]
    if connection_key not in CACHE:
        CACHE[connection_key] = create_connection(
            **settings.DATABASES[connection_key])
    return CACHE[connection_key]


def create_connection(connection_type, params):
    """
    :param connection_type (psql, mysql)
    :type  string
    :values - "psql", "mysql"

    :param params
    :type dict
    :desc Dict with kwargs for connect function
    :return connection object
    """

    def mysql_fix_params(params_to_fix):
        for fix, original in (
                ("db", "database"),
                ("passwd", "password")
        ):
            if original in params_to_fix:
                params_to_fix[fix] = params_to_fix[original]
                del params_to_fix[original]
        return params_to_fix

    fixers = {
        "mysql": mysql_fix_params,
    }

    factories = {
        "mysql": connect_mysql,
        "psql": connect_psql,
        "mssql": connect_mssql
    }

    params = fixers.get(connection_type, lambda x: x)(dict(params.items()))

    return factories[connection_type](**params)
