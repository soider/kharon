# *-* coding: utf-8 *-*
from __future__ import absolute_import
import kharon.settings

from flask import Flask

from flask_peewee.auth import Auth
from flask_peewee.admin import Admin
from peewee import IntegerField, CharField, DateField, BooleanField, DateTimeField, ForeignKeyField
import json

from kharon.web.database import ExDatabase as Database
app = Flask(__name__)

app.config.update(kharon.settings.WEB_SETTINGS)

db = Database(app)
auth = Auth(app, db)
admin = Admin(app, auth, branding="Esavings admin", prefix="/admin")

db.load_database('DATABASE_ORIG')

EsavingsModel = db.get_model_class('DATABASE_ORIG')

class Company(EsavingsModel):
    class Meta:
        db_table = "company"
    title = CharField(verbose_name=u"Название")

    def __unicode__(self):
        return u"%s %s" % (self.title, self.id)


class Object(EsavingsModel):
    class Meta:
        db_table = "object"
    title = CharField(verbose_name=u"Название")

    def __unicode__(self):
        return "%s: %s" % (self.id, self.title)


class System(EsavingsModel):
    class Meta:
        db_table = "system"
    objectid = ForeignKeyField(Object, verbose_name=u"Объект", db_column="objectid")
    stypeid = IntegerField()
    title = CharField(verbose_name=u"Название")
    id = IntegerField()

    def __unicode__(self):
        return u"%s: %s, stypeid %s, objectid %s" % (self.id, self.title, self.stypeid, self.objectid)


class Unit(EsavingsModel):
    class Meta:
        db_table = "unit"
    title = CharField()
    systemid = ForeignKeyField(System, db_column="systemid")
    status = IntegerField()

    def __unicode__(self):
        return u"%s: %s,system %s, status %s" % (self.id, self.title, self.systemid, self.status)

class Param(EsavingsModel):
    class Meta:
        db_table = "param"
    title = CharField()

    def __unicode__(self):
        return u"%s:%s" % (self.id, self.title)

class Meter(EsavingsModel):
    class Meta:
        db_table = "meter"

    title = CharField(verbose_name=u"Счетчик")

    objectid = ForeignKeyField(Object, verbose_name=u"Объект", db_column="objectid")

    def __unicode__(self):
        return u"%s: %s" % (self.id, self.title)


class CarelMeta(db.Model):
    class Meta:
        db_table = "carel"
    carel_host = CharField(verbose_name=u"Хост")
    carel_user = CharField(verbose_name=u"Пользователь")
    carel_password = CharField(verbose_name=u"Пароль")
    carel_db   = CharField(verbose_name=u"База данных")
    carel_idsupervisor = IntegerField(verbose_name=u"Supervisor id")
    carel_idvariable   = IntegerField(verbose_name=u"Variable id")
    esavings_objectid  = ForeignKeyField(Object, db_column="esavings_objectid", verbose_name=u"Object id")
    esavings_unitid    = ForeignKeyField(Unit, db_column="esavings_unitid", verbose_name=u"Unit id")
    esavings_paramid   = ForeignKeyField(Param, db_column="esavings_paramid", verbose_name=u"Param id")
    active             = BooleanField(verbose_name=u"Включен")
    lastsync           = DateTimeField(verbose_name=u"Последний запуск")
    comment            = CharField(verbose_name=u"Примечание", null=True)
    created            = DateTimeField(verbose_name=u"Создан")
    type               = CharField(choices=[("temp", "Temperature"), ("power", "Power")], verbose_name=u"Тип")
    esavings_mdata_meterid = ForeignKeyField(Meter, db_column="esavings_mdata_meterid", verbose_name=u"Mdata meter id", help_text=u"Используется при записи в mdata", null=True)
    company = ForeignKeyField(Company, verbose_name=u"Компания")


class SitMeta(db.Model):
    class Meta:
        db_table = "sit"
    sit_host = CharField(verbose_name=u"Хост")
    sit_user = CharField(verbose_name=u"Пользователь")
    sit_password  = CharField(verbose_name=u"Пароль")
    sit_db =  CharField(verbose_name=u"База данных")
    sit_devcode = IntegerField(verbose_name=u"Dev code")
    sit_parnumber = IntegerField(verbose_name=u"Parnumber")
    esavings_mdata_meterid = ForeignKeyField(Meter, db_column="esavings_mdata_meterid", verbose_name=u"Mdata meter id", help_text=u"Используется при записи в mdata")
    active = BooleanField(verbose_name=u"Включен")
    lastsync = DateTimeField(verbose_name=u"Последний запуск")
    comment = CharField(verbose_name=u"Примечание", null=True)
    created = DateTimeField(verbose_name=u"Создан")
    company = ForeignKeyField(Company, verbose_name=u"Компания")


class DanfossMeta(db.Model):
    class Meta:
        db_table = "danfoss"

    danfoss_host = CharField(verbose_name=u"Хост")
    danfoss_node = IntegerField(verbose_name=u"Нода")
    danfoss_cid      = IntegerField(verbose_name=u"CID")
    danfoss_vid      = IntegerField(verbose_name=u"VID")
    esavings_objectid  = ForeignKeyField(Object, db_column="esavings_objectid", verbose_name=u"Object id")
    esavings_unitid    = ForeignKeyField(Unit, db_column="esavings_unitid", verbose_name=u"Unit id")
    esavings_paramid   = ForeignKeyField(Param, db_column="esavings_paramid", verbose_name=u"Param id")
    active           = BooleanField(verbose_name=u"Включен")
    lastsync         = DateTimeField(verbose_name=u"Последний запуск")
    comment          = CharField(verbose_name=u"Примечание", null=True)
    created          = DateTimeField(verbose_name=u"Создан")
    company = ForeignKeyField(Company, verbose_name=u"Компания")

class DixellMeta(db.Model):
    class Meta:
        db_table = "dixell"

    dixell_url = CharField(verbose_name=u"Хост")
    dixell_value_name = CharField(verbose_name=u"Параметр для выбора")
    esavings_objectid  = ForeignKeyField(Object, db_column="esavings_objectid", verbose_name=u"Object id")
    esavings_unitid    = ForeignKeyField(Unit, db_column="esavings_unitid", verbose_name=u"Unit id", null=True)
    esavings_mdata_meterid = ForeignKeyField(Meter, db_column="esavings_mdata_meterid", verbose_name=u"Mdata meter id", help_text=u"Используется при записи в mdata", null=True)
    esavings_paramid   = ForeignKeyField(Param, db_column="esavings_paramid", verbose_name=u"Param id", null=True)
    esavings_mdata_paramid = ForeignKeyField(Param, db_column="esavings_mdata_paramid", related_name="mdata_paramid_set", verbose_name=u"Mdata paramid", help_text=u"Используется при записи в mdata", null=True)
    type = CharField(choices=[("temp", "Temperature"), ("power", "Power")], verbose_name=u"Тип")
    active = BooleanField(verbose_name=u"Включен")
    lastsync = DateTimeField(verbose_name=u"Последний запуск")
    comment = CharField(verbose_name=u"Примечание", null=True)
    created = DateTimeField(verbose_name=u"Создан")
    esavings_mdata_last = IntegerField(verbose_name=u"Последнее полученное значение")
    company = ForeignKeyField(Company, verbose_name=u"Компания")


from flask_peewee.admin import ModelAdmin

class EsavingsAdmin(ModelAdmin):

    exclude = ("lastsync", "created")
    filter_exclude = ("company__id", "company__title")

    def get_display_name(self):
        return self.admin_name

    def get_admin_name(self):
        return self.get_display_name()

    def get_template_overrides(self):
        return {
            "index": "esavings/admin/index.html",
            "add": "esavings/admin/add.html",
            "edit": "esavings/admin/edit.html",
        }

class CarelAdmin(EsavingsAdmin):
    admin_name = u"Карел"
    columns = ("id", "carel_host", "carel_user", "carel_db", "carel_idsupervisor", "carel_idvariable", "lastsync", "type", "active")

class SitAdmin(EsavingsAdmin):
    admin_name = u"Пирамида"
    columns = ("id", "sit_host", "sit_user", "sit_db", "sit_devcode", "sit_parnumber", "active", "lastsync")

class DanfossAdmin(EsavingsAdmin):
    admin_name = u"Danfoss"
    columns = ("danfoss_host", "danfoss_node", "danfoss_cid", "danfoss_vid", "active", "lastsync")

class DixellAdmin(EsavingsAdmin):
    admin_name = "Dixell"
    exclude = ("lastsync", "created", "esavings_mdata_last")
    columns = ("dixell_url", "dixell_value_name", "esavings_objectid", "esavings_unitid", "esavings_paramid", "type", "esavings_mdata_meterid", "esavings_mdata_paramid", "lastsync")

admin.register(CarelMeta, CarelAdmin)
admin.register(SitMeta, SitAdmin)
admin.register(DanfossMeta, DanfossAdmin)
admin.register(DixellMeta, DixellAdmin)
admin.setup()


@app.route("/get_constraints.json")
def get_constraints():
    units = {}
    meters = {}
    for obj in Object.select():
        for system in obj.system_set:
            for unit in system.unit_set:
                units.setdefault(obj.id, [])
                units[obj.id].append(unit.id)
        for meter in obj.meter_set:
            meters.setdefault(obj.id, [])
            meters[obj.id].append(meter.id)
    return json.dumps({"answer": {"units": units,
                                  "meters": meters}})

if __name__ == "__main__":
    auth.User.create_table(fail_silently=True)
    try:
        app.run("0.0.0.0", port=8080)
    except KeyboardInterrupt:
        app.stop()
