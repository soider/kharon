import peewee
from peewee import *

from flask_peewee.exceptions import ImproperlyConfigured
from flask_peewee.utils import load_class

from flask_peewee.db import Database

class ExDatabase(Database):
    """ Add support for cross-database models"""
    databases = {}

    def load_database(self, name='DATABASE'):
        self.database_config = dict(self.app.config[name])
        try:
            self.database_name = self.database_config.pop('name')
            self.database_engine = self.database_config.pop('engine')
        except KeyError:
            raise ImproperlyConfigured('Please specify a "name" and "engine" for your database')

        try:
            self.database_class = load_class(self.database_engine)
            assert issubclass(self.database_class, peewee.Database)
        except ImportError:
            raise ImproperlyConfigured('Unable to import: "%s"' % self.database_engine)
        except AttributeError:
            raise ImproperlyConfigured('Database engine not found: "%s"' % self.database_engine)
        except AssertionError:
            raise ImproperlyConfigured('Database engine not a subclass of peewee.Database: "%s"' % self.database_engine)
        db = self.database_class(self.database_name, **self.database_config)
        self.databases[name] = db
        self.database = db # for back compability


    def get_model_class(self, database='DATABASE'):
        db = self.databases[database]
        class BaseModel(Model):
            class Meta:
                database = db
        return BaseModel
