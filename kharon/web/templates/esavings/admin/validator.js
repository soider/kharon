<script>
    $(document).on('ready', function(e) {
        var unitsByObject = {}
        var metersByObject = {}
        var objectIdSelector = $("#esavings_objectid");
        var unitOptions = $("#esavings_unitid option")
        var meterOptions = $("#esavings_mdata_meterid option")
        $.ajax('/get_constraints.json',
            {
            'method': 'GET',
            'success': function(data, status) {
                console.log(data)
                unitsByObject = data["answer"]["units"];
                metersByObject = data["answer"]["meters"];
                $('form select[data-role="chosenblank"').chosen();
                objectIdSelector.trigger("change");
                $("#type").trigger('change');
            },
            'dataType': 'json'
        });
        objectIdSelector.on('change', function(e) {
            var oid = objectIdSelector.val()
            unitOptions.each(function(i, el) {
                if (unitsByObject[oid].indexOf(parseInt(el.value)) === -1)  {
                    el.disabled = true;    
                }
                else {
                    el.disabled = false;
                }
            if (unitsByObject[oid][0] == parseInt(el.value)) {
               el.selected = true;
            }

            });
            $("#esavings_mdata_meterid option").each(function(e, el) {
                if (metersByObject[oid].indexOf(parseInt(el.value)) === -1) {
                    el.disabled = true;
                }
                else {
                    el.disabled = false;
                }

                if (metersByObject[oid][0] == parseInt(el.value)) {
                    el.selected = true
                };
            });
            $("#esavings_unitid").trigger("liszt:updated");
            $("#esavings_mdata_meterid").trigger("liszt:updated");
        });

        var typeSelector = $("#type");
        var fieldsByType = {
            "temp": [$("#esavings_paramid")],
            "power": [$("#esavings_mdata_meterid"), $("#esavings_mdata_paramid")] 
        }
        typeSelector.on('change', function(ev) {
            var mode = typeSelector.val();
            if (mode == "power") {
                var mirror = "temp"
            } else {var mirror = "power"}
            for (var idx in fieldsByType[mode]) {
                fieldsByType[mode][idx].parent().parent().show();
            }   
            for (var idx in fieldsByType[mirror]) {
                fieldsByType[mirror][idx].parent().parent().hide();
            }
        });
    });
</script>

 
