# from __future__ import absolute_import

__author__ = 'soider'

INITED = False

if not INITED:
    INITED = True
    from kharon import settings
    import logging
    loggers = settings.LOGGERS
    for logger_name, params in loggers.items():
        logger = logging.getLogger(logger_name)
        for handler, formatter in params["handlers"]:
            logger.addHandler(handler)
            handler.setFormatter(
                formatter
            )
        logger.setLevel(
            params.get("level", settings.DEFAULT_LEVEL)
        )
