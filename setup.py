from setuptools import find_packages, setup

setup(
    name='kharon',
    version='1.72',
    packages=find_packages(),
    url='',
    license='BSD',
    author='Michael Sahnov',
    author_email='sahnov.m@gmail.com',
    description='',
    zip_safe=False,
    include_package_data=True,
    entry_points={
        'console_scripts': [
            "kh_move_carel=kharon.cli.carel:main",
            "kh_move_sit=kharon.cli.sit:main",
            "kh_move_danfoss=kharon.cli.danfoss:main",
            "kh_move_dixell=kharon.cli.dixell:main",
            "kh_web=kharon.cli.web_admin:main"
        ]
    },
    install_requires=['distribute >= 0.7.3',
                      'psycopg2 >= 2.5.1',
                      'MySQL-python >= 1.2.4',
                      'python-dateutil >= 2.1',
                      'pymssql',
                      'flask >= 0.10',
                      'flask-peewee == 0.6.5',
                      'gunicorn >= 18.0',
                      'lxml >= 3.2.3',
                      'gevent >= 0.13.8']
)
